# Udagram

## Description
Udagram is an Instagram clone. Developers push the latest version of their code in a zip file located in a public S3 Bucket. 

Source code location: 

*s3://udacity-demo-1/udacity.zip*.

## Architecture diagram
![Udagram architecture diagram](udagram-architecture-diagram.png)

## Instructions

### Prerequisites
AWS CLI properly configured.

### Build the network infrastructure

Run ```./create.sh UdagramNetwork networking.yml network-parameters.json```

### Build the compute resources

Run ```./create.sh UdagramServers servers.yml servers-parameters.json```

### Update the network infrastructure

Run ```./update.sh UdagramNetwork networking.yml network-parameters.json```

### Update the compute resources

Run ```./update.sh UdagramServers servers.yml servers-parameters.json```

## Live example
[Click here](http://udagr-webap-1no9mhx6crd7k-145466215.us-west-2.elb.amazonaws.com/)
